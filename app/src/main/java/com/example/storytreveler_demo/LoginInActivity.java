package com.example.storytreveler_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginInActivity extends AppCompatActivity {


    DbAdapter dbAdapter;
    String login;
    String password;
    PreferencesAdapter prefAdapter;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefAdapter=new PreferencesAdapter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_in);
        if(prefAdapter.isUser())
        {
            Intent i= new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }

    }
    public void onClickBackPressed(View view)
    {
        Intent i= new Intent(this,WelcomeActivity.class);
        startActivity(i);
        finish();

    }
    public void onClickLoginIn(View view)
    {
        User user;
        //pobranie wpisanego loginu i hasła
        EditText Elogin=(EditText) findViewById(R.id.loginInputLogin);
        EditText Epassword=(EditText) findViewById(R.id.passwordInputLogin);
        TextView loginWarning= (TextView) findViewById(R.id.LoginWarning);
        TextView loginandpasswordWarning=(TextView) findViewById(R.id.LoginInWarning);
        login=Elogin.getText().toString().trim();//przypisanie do tekstu loginu
        password=Epassword.getText().toString();//przypisanie do tekstu hasła
        String warning;
        dbAdapter= new DbAdapter(getApplicationContext());
        dbAdapter.open();
        if(login.equals(""))
        {
           loginWarning.setText("Login nie może być pusty");
        }
        if (password.equals(""))
        {
           loginandpasswordWarning.setText("Hasło nie może być puste");
        }
        if (!password.equals("") && !login.equals(""))
        {
           user=dbAdapter.getUserByLogin(login);
           if(user==null) {
               loginandpasswordWarning.setText("Brak użytkownika o takiej kombinacji loginu i hasła");
           }
           else if(password.equals(user.getPassword()))
           {
               prefAdapter.saveUser(login,password);
               Intent i= new Intent(this, MainActivity.class);
               startActivity(i);
               finish();
           }
           else
           {
               loginandpasswordWarning.setText("Brak użytkownika o takiej kombinacji loginu i hasła");
           }
        }


        dbAdapter.close();



    }
}

