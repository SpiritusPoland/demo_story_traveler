package com.example.storytreveler_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }



    public void onClickBackPressed(View view)
    {
        Intent i= new Intent(this,WelcomeActivity.class);
        startActivity(i);
        finish();

    }

    public void onClickRegister(View view)
    {
        int warns=0;
        String login;
        String password;
        String againPassword;
        String email;

        //pobranie loginu
        EditText edit=(EditText)findViewById(R.id.loginInputRegister);
        login=edit.getText().toString().trim();

        //pobranie hasła
        edit=(EditText)findViewById(R.id.passwordInputRegister);
        password=edit.getText().toString().trim();

        //pobranie powtórzonego hasła
        edit=(EditText)findViewById(R.id.passwordAgainInputRegister);
        againPassword=edit.getText().toString().trim();

        //pobranie emaila
        edit=(EditText)findViewById(R.id.emailInputRegister);
        email=edit.getText().toString().trim();

        if(login.equals(""))
        {
            TextView loginWarn=(TextView) findViewById(R.id.login_warning_Register);
            loginWarn.setText(R.string.noLogin);
            warns++;
        }
        if(password.equals(""))// jeśli hasło puste
        {
            TextView passwordWarn=(TextView)findViewById(R.id.password_warning_Register);
            passwordWarn.setText(R.string.noPassword);
            warns++;
        }
        if(againPassword.equals(""))//jeśli nie powtórzono hasła
        {
            TextView againPasswordWarn=(TextView)findViewById(R.id.passwordAgain_warning_Register);
            againPasswordWarn.setText(R.string.noAgainPassword);
            warns++;
        }
        if(!password.equals("") && !againPassword.equals("") && !password.equals(againPassword))//jeśli nie są puste ale też nie pasują
        {
            TextView passwordWarn=(TextView)findViewById(R.id.password_warning_Register);
            passwordWarn.setText(R.string.passwordsdoesntmatch);
            warns++;
        }
        if(email.equals(""))
        {
            TextView emailWarn=(TextView)findViewById(R.id.email_warning_register);
            emailWarn.setText(R.string.noEmail);
            warns++;
        }
        if(warns==0) {

        DbAdapter db=new DbAdapter(getApplicationContext());
        db.open();


        //sprawdzenie czy istnieje użytkownik o podanym loginie

        User user=db.getUserByLogin(login);

        if(user!=null)
        {
            TextView loginWarn=(TextView) findViewById(R.id.login_warning_Register);
            loginWarn.setText(R.string.noLogin);
            warns++;
        }

        user=db.getUserByEmail(email);
        if (user!=null)
        {
            TextView emailWarn=(TextView)findViewById(R.id.email_warning_register);
            emailWarn.setText(R.string.noEmail);
            warns++;
        }

        if (user==null && warns==0)
        {
            db.insertUser(login,password,email);
            Intent i= new Intent(this, RegisteredActivity.class);
            startActivity(i);
            finish();
        }

        db.close();


        }



    }




}
