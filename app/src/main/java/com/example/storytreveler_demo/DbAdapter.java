package com.example.storytreveler_demo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public class DbAdapter {
    private static final String DEBUG_TAG = "SQLiteDbMenager";
    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "storytraveler.db";

    //TABELA USERS//
    private static final String DB_USERS_TABLE = "users";

    public static final String USERS_KEY_USER_ID = "user_id";
    public static final String USERS_USER_ID_OPTIONS = "INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT";
    public static final int USERS_USER_ID_COLUMN = 0;

    public static final String USERS_KEY_LOGIN = "login";
    public static final String USERS_LOGIN_OPTIONS = "VARCHAR(45) UNIQUE NOT NULL";
    public static final int USERS_LOGIN_COLUMN = 1;

    public static final String USERS_KEY_PASSWORD = "password";
    public static final String USERS_PASSWORD_OPTIONS = "VARCHAR(60) NOT NULL";
    public static final int USERS_PASSWORD_COLUMN = 2;

    public static final String USERS_KEY_EMAIL = "email";
    public static final String USERS_EMAIL_OPTIONS = "VARCHAR(60) UNIQUE NOT NULL";
    public static final int USERS_EMAIL_COLUMN = 3;

    public static final String DB_CREATE_USERS_TABLE =
            "CREATE TABLE  IF NOT EXISTS " + DB_USERS_TABLE + "( "
                    + USERS_KEY_USER_ID + " " + USERS_USER_ID_OPTIONS + ", "
                    + USERS_KEY_LOGIN + " " + USERS_LOGIN_OPTIONS + ", "
                    + USERS_KEY_PASSWORD + " " + USERS_PASSWORD_OPTIONS + ", "
                    + USERS_KEY_EMAIL + " " + USERS_EMAIL_OPTIONS + ");";
    private static final String DB_DROP_USERS_TABLE =
            "DROP TABLE IF EXISTS " + DB_USERS_TABLE;


    // TABELA STORIES//
    private static final String DB_STORIES_TABLE = "stories";

    public static final String STORIES_KEY_STORY_ID = "story_id";
    public static final String STORIES_STORY_ID_OPTIONS = "INTEGER PRIMARY KEY NOT NULL AUTOINCREMENT";
    public static final int STORIES_STORY_ID_COLUMN = 0;

    public static final String STORIES_KEY_TITLE = "title";
    public static final String STORIES_TITLE_OPTIONS = "VARCHAR(45) NOT NULL";
    public static final int STORIES_TITLE_COLUMN = 1;

    public static final String STORIES_KEY_DESCRIPTION="description";
    public static final String STORIES_DESCRIPTION_OPTIONS="TEXT NOT NULL";
    public static final int STORIES_DESCRIPTION_COLUMN=2;





    public static final String DB_CREATE_STORIES_TABLE =
            "CREATE TABLE  IF NOT EXISTS " + DB_STORIES_TABLE + "( "
                    + STORIES_KEY_STORY_ID + " " + STORIES_STORY_ID_OPTIONS + ", "
                    + STORIES_KEY_TITLE + " " + STORIES_TITLE_OPTIONS + ", "
                    + STORIES_KEY_DESCRIPTION + " " + STORIES_DESCRIPTION_OPTIONS + ");";

    //TABELA STAGES//
    private static final String DB_STAGES_TABLE = "stages";

    public static final String STAGES_KEY_STAGE_ID = "stage_id";
    public static final String STAGES_STAGE_ID_OPTIONS = "INTEGER NOT NULL AUTOINCREMENT PRIMARY KEY";
    public static final int STAGES_STAGE_ID_COLUMN = 0;

    public static final String STAGES_KEY_STAGE_TITLE ="title";
    public static final String STAGES_STAGE_TITLE_OPTIONS = "VARCHAR(60) NULL";
    public static final int STAGES_STAGE_TITLE_COLUMM = 1;

    public static final String  STAGES_KEY_DESCRIPTION = "description";
    public static final String STAGES_DESCRIPTION_OPTIONS = "TEXT";
    public static final int STAGES_DESCRIPTION_COLUMN = 2;

    public static final String STAGES_KEY_LATITUDE="latitude";
    public static final String STAGES_LATITUDE_OPTIONS="DOUBLE";
    public static final int STAGES_LATITUDE_COLUMN=3;

    public static final String STAGES_KEY_LONGITUDE="longitude";
    public static final String STAGES_LONGITUDE_OPTIONS="DOUBLE";
    public static final int STAGES_LONGITUDE_COLUMN=4;

    public static final String STAGES_KEY_TOLERANCE="tolerance";
    public static final String STAGES_TOLERANCE_OPTIONS="DOUBLE";
    public static final int STAGES_TOLERANCE_COLUMN=5;

    public static final String STAGES_KEY_STORY_ID = "story_id";
    public static final String STAGES_STORY_ID_OPTIONS="INTEGER NOT NULL, FOREIGN KEY("+STAGES_KEY_STORY_ID+") REFERENCES "+ DB_STORIES_TABLE+"("+STORIES_KEY_STORY_ID+")";
    public static final int STAGES_STORY_ID_COLUMN=6;


    public static final String DB_CREATE_STAGES_TABLE =
            "CREATE TABLE  IF NOT EXISTS " + DB_STAGES_TABLE + "( "
                    + STAGES_KEY_STAGE_ID + " " + STAGES_STAGE_ID_OPTIONS + ", "
                    + STAGES_KEY_STAGE_TITLE + " " + STAGES_STAGE_TITLE_OPTIONS  + ", "
                    + STAGES_KEY_DESCRIPTION + " " + STAGES_DESCRIPTION_OPTIONS + ", "
                    + STAGES_KEY_LATITUDE + " " + STAGES_LATITUDE_OPTIONS + ", "
                    + STAGES_KEY_LONGITUDE + " " + STAGES_LONGITUDE_OPTIONS + ", "
                    + STAGES_KEY_TOLERANCE+ " " + STAGES_TOLERANCE_OPTIONS + ", "
                    + STAGES_KEY_STORY_ID + " " + STAGES_STORY_ID_OPTIONS +  ");";





    //TABELA GAMES
    public static final String DB_GAMES_TABLE="games";

    public static final String GAMES_KEY_GAME_ID="game_id";
    public static final String GAMES_GAME_ID_OPTIONS="INTEGER NOT NULL AUTOINCREMENT PRIMARY KEY";
    public static final int GAMES_GAME_ID_COLUMN=0;

    public static final String GAMES_KEY_STORY_ID = "story_id";
    public static final String GAMES_STORY_ID_OPTIONS="INTEGER NOT NULL, FOREIGN KEY(" +GAMES_KEY_STORY_ID + ") REFERENCES " +DB_STORIES_TABLE+"(" + STORIES_KEY_STORY_ID + ")";
    public static final int GAMES_STORY_COLUMN = 1;

    public static final String GAMES_KEY_USER_ID = "user_id";
    public static final String GAMES_USER_ID_OPTIONS = "INTEGER NOT NULL, FOREIGN KEY(" +GAMES_KEY_USER_ID + ") REFERENCES " + DB_USERS_TABLE + "("+USERS_KEY_USER_ID+")";
    public static final int GAMES_USER_ID_COLUMN = 2;

    public static final String GAMES_KEY_TIME_START= "time_start";
    public static final String GAMES_TIME_START_OPTIONS="TIMESTAMP NULL";
    public static final int GAMES_TIME_START_COLUMN = 3;

    public static final String GAMES_KEY_TIME_END= "time_end";
    public static final String GAMES_TIME_END_OPTIONS="TIMESTAMP NULL";
    public static final int GAMES_TIME_END_COLUMN = 4;

    public static final String DB_CREATE_GAMES_TABLE =
            "CREATE TABLE  IF NOT EXISTS " + DB_GAMES_TABLE + "( "
                    + GAMES_KEY_GAME_ID + " " + GAMES_GAME_ID_OPTIONS + ", "
                    + GAMES_KEY_STORY_ID + " " + GAMES_STORY_ID_OPTIONS  + ", "
                    + GAMES_KEY_USER_ID + " " + GAMES_USER_ID_OPTIONS + ", "
                    + GAMES_KEY_TIME_START + " " + GAMES_TIME_START_OPTIONS + ", "
                    + GAMES_KEY_TIME_END + " " + GAMES_TIME_END_OPTIONS + ");";



    //TABELA CHAPTERS//
    public static final String DB_CHAPTERS_TABLE="chapters";

    public static final String CHAPTERS_KEY_CHAPTER_ID = "chapter_id";
    public static final String CHAPTERS_CHAPTER_ID_OPTIONS = "INTEGER NOT NULL AUTOINCREMENT PRIMARY KEY";
    public static final int    CHAPTERS_CHAPTER_ID_COLUMN = 0;

    public static final String CHAPTERS_KEY_STAGE_ID = "stage_id";
    public static final String CHAPTERS_STAGE_ID_OPTIONS = "INTEGER NOT NULL, FOREIGN KEY("+STAGES_KEY_STAGE_ID+") REFERENCES "+DB_STAGES_TABLE + "("+STAGES_KEY_STAGE_ID+")";
    public static final int    CHAPTERS_STAGE_ID_COLUMN = 1;

    public static final String CHAPTERS_KEY_GAME_ID="game_id";
    public static final String CHAPTERS_GAME_ID_OPTIONS="INTEGER NOT NULL FOREIGN KEY("+GAMES_KEY_GAME_ID+") REFERENCES "+DB_GAMES_TABLE+"("+ GAMES_KEY_GAME_ID+")";
    public static final int  CHAPTERS_GAME_ID_OOLUMN=2;

    public static final String CHAPTERS_KEY_TIME_START = "time_start";
    public static final String CHAPTERS_TIME_START_OPTIONS = "TIMESTAMP NULL";
    public static final int    CHAPTERS_TIME_START_COLUMN = 3;

    public static final String CHAPTERS_KEY_TIME_END = "time_end";
    public static final String CHAPTERS_TIME_END_OPTIONS = "TIMESTAMP NULL";
    public static final int    CHAPTERS_TIME_END_COLUMN = 4;



    public static final String DB_CREATE_CHAPTERS_TABLE =
            "CREATE TABLE  IF NOT EXISTS " + DB_CHAPTERS_TABLE + "( "
                    + CHAPTERS_KEY_CHAPTER_ID + " " + CHAPTERS_CHAPTER_ID_OPTIONS + ", "
                    + CHAPTERS_KEY_STAGE_ID + " " + CHAPTERS_STAGE_ID_OPTIONS  + ", "
                    + CHAPTERS_KEY_GAME_ID + " " + CHAPTERS_GAME_ID_OPTIONS + ", "
                    + CHAPTERS_KEY_TIME_START + " " + CHAPTERS_TIME_START_OPTIONS + ", "
                    + CHAPTERS_KEY_TIME_END + " " + CHAPTERS_TIME_END_OPTIONS +");";




    private static final String DB_CREATE_DATABASE=DB_CREATE_USERS_TABLE+"\n"+DB_CREATE_STORIES_TABLE+ "\n"+ DB_CREATE_STAGES_TABLE+"\n"+DB_CREATE_GAMES_TABLE+"\n"+DB_CREATE_CHAPTERS_TABLE;


    //
    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
            super(context, name, factory, version);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DB_CREATE_DATABASE);
            Log.d(DEBUG_TAG,"DateBase creating...");
            Log.d(DEBUG_TAG,"Table "+DB_USERS_TABLE + "ver." + DB_VERSION + "created");
            Log.d(DEBUG_TAG, DB_CREATE_USERS_TABLE);
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL(DB_DROP_USERS_TABLE);

            Log.d(DEBUG_TAG, "Database updating...");
            Log.d(DEBUG_TAG, "Table " + DB_USERS_TABLE + " updated from ver." + oldVersion + " to ver." + newVersion);
            Log.d(DEBUG_TAG, "All data is lost.");

            onCreate(db);
        }
    }
    public DbAdapter(Context context)
    {
        this.context=context;
    }

    public DbAdapter open()
    {
        dbHelper = new DatabaseHelper(context, DB_NAME, null, DB_VERSION);
        try {
            db = dbHelper.getWritableDatabase();
        } catch (SQLException e) {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public  void close()
    {
        dbHelper.close();
    }

    //ADDING USER
    public long insertUser(String login,String password, String email)
    {
        ContentValues newUser = new ContentValues();
        newUser.put(USERS_KEY_LOGIN,login);
        newUser.put(USERS_KEY_EMAIL,email);
        newUser.put(USERS_KEY_PASSWORD,password);
        return db.insert(DB_USERS_TABLE,null,newUser);
    }
    public boolean deleteUser(long id)
    {
        String where = USERS_KEY_USER_ID + "=" + id;
        return db.delete(DB_USERS_TABLE, where,null)>0;
    }

    public User getUserByLogin(String login)
    {
        String[] columns={USERS_KEY_USER_ID, USERS_KEY_LOGIN,  USERS_KEY_PASSWORD, USERS_KEY_EMAIL,};
        String where= USERS_KEY_LOGIN+ "= '"+ login+"'";
        Cursor cursor= db.query(DB_USERS_TABLE,columns,where,null,null,null,null,null);
       // Cursor cursor= db.query("users",columns,"login = 'ala'",null,null,null,null);
        User user = null;
        if (cursor!= null && cursor.moveToFirst())
        {
            long id=cursor.getInt(USERS_USER_ID_COLUMN);
            String password = cursor.getString(USERS_PASSWORD_COLUMN);
            String email = cursor.getString(USERS_EMAIL_COLUMN);
            user=new User(id,login,email,password);
        }
        return user;

    }
    public User getUserByEmail(String email)
    {
        String[] columns={USERS_KEY_USER_ID, USERS_KEY_LOGIN,  USERS_KEY_PASSWORD, USERS_KEY_EMAIL,};
        String where= USERS_KEY_EMAIL +"= '" + email+"'";
        Cursor cursor= db.query(DB_USERS_TABLE,columns,where,null,null,null,null);
        User user = null;
        if (cursor!= null && cursor.moveToFirst())
        {
            long id=cursor.getInt(USERS_USER_ID_COLUMN);
            String password = cursor.getString(USERS_PASSWORD_COLUMN);
            String login= cursor.getString(USERS_LOGIN_COLUMN);
            user=new User(id,login,email,password);
        }
        return user;

    }
    public void addUser (String login, String password, String email)
    {


    }



}