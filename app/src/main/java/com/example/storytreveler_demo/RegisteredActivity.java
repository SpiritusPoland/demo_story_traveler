package com.example.storytreveler_demo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RegisteredActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registered);
    }




    public void onClickBackToLogin(View view)
    {
        Intent i= new Intent(this, LoginInActivity.class);
        startActivity(i);
        finish();
    }
}
