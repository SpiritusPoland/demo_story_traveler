package com.example.storytreveler_demo;

public class User {


    private long id;
    private String login;
    private String email;
    private String password;

    public User(long id, String login, String email, String password)
    {
        this.id=id;
        this.login=login;
        this.email=email;
        this.password=password;
    }

    public long getId()
    {
        return id;
    }
    public String getLogin()
    {
        return login;
    }
    public void setPassword(String password)
    {
        this.password=password;
    }
    public String getPassword ()
    {
        return password;
    }


}
