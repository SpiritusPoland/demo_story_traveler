package com.example.storytreveler_demo;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class PreferencesAdapter{

    SharedPreferences myPreferences;
    SharedPreferences.Editor myEditor;

    public PreferencesAdapter(Context context)
    {

        myPreferences= PreferenceManager.getDefaultSharedPreferences(context);
        myEditor= myPreferences.edit();
    }


    public void saveUser(String user, String password)
    {
        myEditor.putString("login",user);
        myEditor.putString("password",password);
        myEditor.commit();

    }
    public boolean isUser()
    {
       String login= myPreferences.getString("login",null);
       String password= myPreferences.getString("password",null);
       if(login!=null && password!=null)
           return true;
       else
           return false;
    }
    public String getUser()
    {
        return myPreferences.getString("login",null);
    }
    public String getPassword()
    {
        return myPreferences.getString("password", null);
    }
}
