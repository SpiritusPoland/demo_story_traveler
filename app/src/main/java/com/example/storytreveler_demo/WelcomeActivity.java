package com.example.storytreveler_demo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class WelcomeActivity extends AppCompatActivity {

    PreferencesAdapter prefAdapter;

    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        prefAdapter=new PreferencesAdapter(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        if(prefAdapter.isUser())
        {
            Intent i= new Intent(this, MainActivity.class);
            startActivity(i);
            finish();
        }



    }

    public void onClickLogin(View view)
    {
     context=getApplicationContext();
     Intent intent = new Intent(context, LoginInActivity.class);
     startActivity(intent);
    }
    public void onClickRegister(View view)
    {
        context=getApplicationContext();
        Intent intent = new Intent(context, RegisterActivity.class);
        startActivity(intent);

    }

}
